//
//  Color+.swift
//  My Taxi
//
//  Created by Shokhod on 16/12/21.
//

import Foundation
import UIKit

extension UIColor {
    
    static let themeColor = ThemeColor()
    
}

struct ThemeColor {
    let textColor = UIColor(named: "text_Color")
    let borderColor = UIColor(named: "border_Color")
}

