//
//  TripHistoryModel.swift
//  My Taxi
//
//  Created by Shokhod on 17/12/21.
//

import Foundation
import UIKit

struct TripHistoryModel {
    let wayFrom: String
    let wayTo  : String
    let time   : String
    let carType: CarType
}

enum CarType {
    case gray, yellow, black

    var image: UIImage {
        switch self {
            case .gray: return UIImage(named: "black_car")!
            case .yellow: return UIImage(named: "gray_car")!
            case .black: return UIImage(named: "yellow_car")!
        }
    }
}

var mockData: [TripHistoryModel] = [
    TripHistoryModel(wayFrom: "улица Sharof Rashidov, Ташкент", wayTo: "5a улица Асадуллы Ходжаева", time: "21:36 - 22:12", carType: .gray),
    TripHistoryModel(wayFrom: "улица Sharof Rashidov, Ташкент", wayTo: "5a улица Асадуллы Ходжаева", time: "21:36 - 22:12", carType: .yellow),
    TripHistoryModel(wayFrom: "улица Sharof Rashidov, Ташкент", wayTo: "5a улица Асадуллы Ходжаева", time: "21:36 - 22:12", carType: .black),
    TripHistoryModel(wayFrom: "улица Sharof Rashidov, Ташкент", wayTo: "5a улица Асадуллы Ходжаева", time: "21:36 - 22:12", carType: .black),
    TripHistoryModel(wayFrom: "улица Sharof Rashidov, Ташкент", wayTo: "5a улица Асадуллы Ходжаева", time: "21:36 - 22:12", carType: .yellow)
]
