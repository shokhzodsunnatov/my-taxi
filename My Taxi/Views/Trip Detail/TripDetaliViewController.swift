//
//  TripDetaliViewController.swift
//  My Taxi
//
//  Created by Shokhod on 22/12/21.
//

import UIKit
import GoogleMaps
import SnapKit

class TripDetaliViewController: UIViewController {
    
    //MARK: Views PROPERTIES
    private let scrollView = UIScrollView()
    private let carInfoView = CarInfoView.instanceFromNib() as! CarInfoView
    private let driverInfoView = DriverInfoView.instanceFromNib()
    private let generalInfoView = GeneralInfoView.instanceFromXib()
    private let costInfoView = CostInfoView.instanceFromXib()
    private let miniMapView = GMSMapView()
    
    let helpButton: VerticalButton = {
        let button = VerticalButton()
        button.highlightedColor = UIColor(named: "help_btn_tint")
        button.setTitle("Помощь", for: .normal)
        button.setImage(UIImage(systemName: "questionmark.circle.fill"), for: .normal)
        button.adjustsImageWhenHighlighted = false
        return button
    }()
    
    let repeatButton: VerticalButton = {
        let button = VerticalButton()
        button.highlightedColor = UIColor(named: "repeat_btn_tint")
        button.setTitle("Повторить", for: .normal)
        button.setImage(UIImage(named: "repeat"), for: .normal)
        button.adjustsImageWhenHighlighted = false
        return button
    }()
    
    let callButton: VerticalButton = {
        let button = VerticalButton()
        button.highlightedColor = UIColor(named: "call_btn_tint")
        button.setTitle("Позвонить", for: .normal)
        button.setImage(UIImage(systemName: "phone.fill"), for: .normal)
        button.adjustsImageWhenHighlighted = false
        return button
    }()
    
    let detailView: UIView = {
        let view = UIView()
        view.backgroundColor = .red
        return view
    }()
    
    let deleteInfoButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(systemName: "trash.fill"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
        button.adjustsImageWhenHighlighted = false
        button.setTitle("Удалить данные", for: .normal)
        button.tintColor = .red.withAlphaComponent(0.7)
        button.backgroundColor = UIColor.red.withAlphaComponent(0.2)
        button.setTitleColor(UIColor.red.withAlphaComponent(0.7), for: .normal)
        button.setTitleColor(UIColor.red, for: .highlighted)
        button.layer.cornerRadius = 12
        return button
    }()
    
    let backButton: UIButton = {
        let button = UIButton(type: .system)
        button.frame = CGRect(x: .zero, y: .zero, width: 38, height: 38)
        button.setImage(
            UIImage(systemName: "arrow.left",
                    withConfiguration: UIImage.SymbolConfiguration(
                        pointSize: 18,
                        weight: .regular)
                   ), for: .normal)
        button.tintColor = .black
        button.backgroundColor = .white
        button.layer.cornerRadius = button.frame.width/2
        button.layer.shadowColor = UIColor.black.cgColor
        button.layer.shadowOpacity = 0.5
        button.layer.shadowOffset = CGSize(width: 1, height: 1)
        button.layer.shadowRadius = 1.5
        return button
    }()
    
    var geoJson: [[CLLocationDegrees]] = [
    [69.271737, 41.317323],
    [69.272735, 41.317061],
    [69.272671, 41.316836],
    [69.275874, 41.316134],
    [69.276620, 41.317819],
    [69.274375, 41.318330],
    [69.274541, 41.318733]
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        backButton.addTarget(self, action: #selector(backBtnTapped), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = true
    }
    
    @objc func backBtnTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupViews() {
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.contentInsetAdjustmentBehavior = .never
        
        //MARK: Adding Views
        view.addSubview(scrollView)
        view.addSubview(backButton)
        scrollView.addSubview(detailView)
        
        scrollView.addSubview(miniMapView)
        drawPolyline()
        
        detailView.addSubview(carInfoView)
        detailView.addSubview(helpButton)
        detailView.addSubview(repeatButton)
        detailView.addSubview(callButton)
        detailView.addSubview(driverInfoView)
        detailView.addSubview(generalInfoView)
        detailView.addSubview(costInfoView)
        detailView.addSubview(deleteInfoButton)
        
        //MARK: SnapKits
        scrollView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        scrollView.backgroundColor = .white
        
        miniMapView.snp.makeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalTo(320)
            make.right.left.equalToSuperview()
            make.top.equalTo(scrollView.safeAreaInsets.top)
        }
        
        backButton.snp.makeConstraints { make in
            make.width.height.equalTo(40)
            make.top.equalTo(view.safeAreaLayoutGuide)
            make.left.equalToSuperview().offset(16)
        }
        
        detailView.snp.makeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalTo(860)
            make.top.equalTo(miniMapView.snp.bottom).offset(-10)
            make.bottom.right.left.equalToSuperview()
        }
        detailView.backgroundColor = .white
        detailView.layer.cornerRadius = 12
        
        carInfoView.snp.makeConstraints { make in
            make.height.equalTo(135)
            make.top.equalToSuperview().offset(20)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
        }
        
        helpButton.snp.makeConstraints { make in
            make.top.equalTo(carInfoView.snp.bottom).offset(18)
            make.left.equalToSuperview().offset(16)
            make.height.equalTo(56)
            make.width.equalToSuperview().multipliedBy(0.29)
        }
        helpButton.layer.cornerRadius = 12
        
        repeatButton.snp.makeConstraints { make in
            make.top.equalTo(carInfoView.snp.bottom).offset(18)
            make.height.equalTo(56)
            make.width.equalToSuperview().multipliedBy(0.29)
            make.centerX.equalToSuperview()
        }
        repeatButton.layer.cornerRadius = 12
        
        callButton.snp.makeConstraints { make in
            make.top.equalTo(carInfoView.snp.bottom).offset(18)
            make.height.equalTo(56)
            make.width.equalToSuperview().multipliedBy(0.29)
            make.right.equalToSuperview().offset(-16)
        }
        callButton.layer.cornerRadius = 12
        
        driverInfoView.snp.makeConstraints { make in
            make.height.equalTo(101)
            make.top.equalTo(callButton.snp.bottom).offset(28)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
        }
        
        generalInfoView.snp.makeConstraints { make in
            make.top.equalTo(driverInfoView.snp.bottom).offset(24)
            make.height.equalTo(178)
            make.right.equalToSuperview().offset(-16)
            make.left.equalToSuperview().offset(16)
        }
        
        costInfoView.snp.makeConstraints { make in
            make.top.equalTo(generalInfoView.snp.bottom).offset(24)
            make.height.equalTo(165)
            make.right.equalToSuperview().offset(-16)
            make.left.equalToSuperview().offset(16)
        }
        
        deleteInfoButton.snp.makeConstraints { make in
            make.top.equalTo(costInfoView.snp.bottom).offset(24)
            make.height.equalTo(50)
            make.right.equalToSuperview().offset(-16)
            make.left.equalToSuperview().offset(16)
        }
    }
}

//MARK: Google Map
extension TripDetaliViewController {
    
   
    func drawPolyline() {
        let coordinates = self.geoJson.map({ CLLocationCoordinate2D(latitude: $0.last!, longitude: $0.first!) })
        let path = GMSMutablePath()
        
        for line in coordinates {
                path.add(line)
        }
        
        var marker = GMSMarker(position: coordinates.first!) // ! -> just for now its bad sign
        marker.icon = self.imageWithImage(image: UIImage(named: "red")!, scaledToSize: CGSize(width: 24, height: 24))
        marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        marker.isFlat = true
        marker.map = self.miniMapView
        
        marker = GMSMarker(position: coordinates.last!)
        marker.icon = self.imageWithImage(image: UIImage(named: "blue")!, scaledToSize: CGSize(width: 24, height: 24))
        marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        marker.isFlat = true
        marker.map = self.miniMapView
        
        let polyline = GMSPolyline(path: path)
        polyline.strokeColor = UIColor(named: "lineColor") ?? .blue
        polyline.strokeWidth = 3.0
        polyline.map = self.miniMapView
        
        let bounds = GMSCoordinateBounds(path: path)
        self.miniMapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 70.0))
        //TODO: UnCommite it
        miniMapView.settings.setAllGesturesEnabled(false)
    }

    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
}

