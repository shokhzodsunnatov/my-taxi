//
//  CarInfoView.swift
//  My Taxi
//
//  Created by Shokhod on 22/12/21.
//

import UIKit
import SnapKit

class CarInfoView: UIView {
    
    @IBOutlet weak var carNumberView: UIView!
    @IBOutlet weak var regionNumberLB: UILabel!
    @IBOutlet weak var numberCar: UILabel!
    @IBOutlet weak var dividerView: UIView!
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "CarInfoView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        carNumberView.backgroundColor = .white
        carNumberView.layer.cornerRadius = 6
        carNumberView.layer.borderColor  = UIColor.gray.withAlphaComponent(0.5).cgColor
        carNumberView.layer.borderWidth  = 1
        
        regionNumberLB.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(5)
            make.right.left.equalToSuperview().offset(6)
            make.bottom.equalToSuperview().offset(-6)
        }
        
        dividerView.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        
        carNumberView.layer.shadowColor = UIColor.black.withAlphaComponent(0.3).cgColor
        carNumberView.layer.shadowOpacity = 0.5
        carNumberView.layer.shadowOffset = CGSize(width: 1, height: 2)
        carNumberView.layer.shadowRadius = 3
        
    }
    
}
