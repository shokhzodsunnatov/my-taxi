//
//  GeneralInfoView.swift
//  My Taxi
//
//  Created by Shokhod on 22/12/21.
//

import UIKit

class GeneralInfoView: UIView {
    
    class func instanceFromXib() -> UIView {
        return UINib(nibName: "GeneralInfoView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    
}
