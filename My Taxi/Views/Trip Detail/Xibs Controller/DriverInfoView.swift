//
//  DriverInfoView.swift
//  My Taxi
//
//  Created by Shokhod on 22/12/21.
//

import UIKit

class DriverInfoView: UIView {

    class func instanceFromNib() -> UIView {
        return UINib(nibName: "DriverInfoView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }

}
