//
//  ContainerViewController.swift
//  My Taxi
//
//  Created by Shokhod on 21/12/21.
//

import UIKit
import SnapKit

class ContainerViewController: UIViewController {
    
    enum MenuState {
        case opened
        case closed
    }
    
    private var menuState: MenuState = .closed
    
    let menuVC = MenuViewController()
    let mapVC = MapViewController()
    let tripHistoryVC = TripHistoryViewController()
    var navVC: UINavigationController? 
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.backButtonTitle = "Мои поездки"
        self.navigationController?.navigationBar.tintColor = UIColor(named: "text_Color")
        
        
        addChildVCs()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = true
    }
    
    private func addChildVCs() {
        // Menu
        menuVC.delegate = self
        addChild(menuVC)
        view.addSubview(menuVC.view)
        menuVC.didMove(toParent: self)
        
        // Home
        mapVC.delegate = self
        let navVC = UINavigationController(rootViewController: mapVC)
        addChild(navVC)
        view.addSubview(navVC.view)
        navVC.didMove(toParent: self)
        self.navVC = navVC
    }
    
}

extension ContainerViewController: MapViewControlllerDelegate {
    func didTapMenuButton() {
        toggleMenu(complation: nil)
    }
    
    private func toggleMenu(complation: (()-> Void)? ) {
        switch menuState {
        case .closed:
            UIView.animate(
                withDuration: 0.5,
                delay: 0,
                usingSpringWithDamping: 0.8,
                initialSpringVelocity: 0,
                options: .curveEaseOut) {
                    
                    self.navVC?.view.frame.origin.x = self.mapVC.view.frame.size.width - 100
                    self.navVC?.view.frame.origin.y = 40
                    self.navVC?.view.layer.cornerRadius = 20
                    self.navVC?.view.frame.size.height = self.mapVC.view.frame.size.height - 86
                    
                } completion: { [weak self] done in
                    if done {
                        self?.menuState = .opened
                    }
                }
            
        case .opened:
            UIView.animate(
                withDuration: 0.5,
                delay: 0,
                usingSpringWithDamping: 0.8,
                initialSpringVelocity: 0,
                options: .curveEaseOut) {
                    
                    self.navVC?.view.frame.origin.x = 0
                    self.navVC?.view.layer.cornerRadius = 0
                    self.navVC?.view.frame.origin.y = 0
                    self.navVC?.view.frame.size.height = (self.navVC?.view.frame.size.height)! + 86
                    
                } completion: { [weak self] done in
                    if done {
                        self?.menuState = .closed
                        DispatchQueue.main.async {
                            complation?()
                        }
                    }
                }
        }
    }
}

extension ContainerViewController: MenuViewControllerDelegate {
    
    func didSelect(menuItem: MenuViewController.MenuOptionals) {
        toggleMenu { [weak self] in
            switch menuItem {
            case .info:
                self?.goToTripHistoryView()
            case .payType:
                print("Tapper Pay cell")
            case .favAdrress:
                print("Tapper fav adress")
            }
        }
    }
    
    func goToTripHistoryView() {
        let vc = tripHistoryVC
        
        navigationController?.pushViewController(vc, animated: true)
        
//        mapVC.addChild(vc)
//        mapVC.view.addSubview(vc.view)
//        vc.view.frame = view.frame
//        vc.didMove(toParent: mapVC)
//        homeVC.title = vc.title
    }
    
//    func resetToMap() {
//        tripHistoryVC.view.removeFromSuperview()
//        tripHistoryVC.didMove(toParent: nil)
//    }
    
}
