//
//  UserInfoView.swift
//  My Taxi
//
//  Created by Shokhod on 22/12/21.
//

import UIKit

class UserInfoView: UIView {

    @IBOutlet weak var userImagView: UIImageView!
    @IBOutlet weak var userFullNameLB: UILabel!
    @IBOutlet weak var userPhoneNumberLB: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    func commonInit() {
        let viewFrameXib = Bundle.main.loadNibNamed("UserInfoView", owner: self, options: nil)![0] as! UIView
        viewFrameXib.frame = self.bounds
        addSubview(viewFrameXib)
    }
    
    
}
