//
//  MenuViewController.swift
//  SideMenu
//
//  Created by Shokhod on 19/12/21.
//

import UIKit
import SnapKit

protocol MenuViewControllerDelegate: AnyObject {
    func didSelect(menuItem: MenuViewController.MenuOptionals)
}

class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    weak var delegate: MenuViewControllerDelegate?
    
    enum MenuOptionals: String, CaseIterable {
        case info = "Мои поездки"
        case payType = "Способы оплаты "
        case favAdrress = "Избранные адреса"
    }
    
    let menuImages = ["myHistory", "wallet", "favourite"]
    
    private let tableView: UITableView = {
        let table = UITableView()
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        return table
    }()
    
    private let cellSeparateColor = UIColor(named: "cell_Separate")
    private let sideMenuBG = UIColor(named: "sideMenu_bg")
    private let tableViewHeader = UIColor(named: "tableHeaderView_bg")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = nil
        tableView.separatorColor = cellSeparateColor
        view.backgroundColor = sideMenuBG
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = CGRect(x: 0, y: view.safeAreaInsets.top, width: view.bounds.size.width - 136, height: view.bounds.size.height)
    }
    
    //Table
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MenuOptionals.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.selectionStyle = .none
        cell.textLabel?.text = MenuOptionals.allCases[indexPath.row].rawValue
        cell.textLabel?.textColor = .white
        cell.imageView?.image = UIImage(named: menuImages[indexPath.row])
        cell.backgroundColor = sideMenuBG
        cell.contentView.backgroundColor = sideMenuBG
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = MenuOptionals.allCases[indexPath.row]
        delegate?.didSelect(menuItem: item)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        100
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        let myHeader = UserInfoView()
            
        myHeader.layer.cornerRadius = 24
            headerView.addSubview(myHeader)
            
            myHeader.snp.makeConstraints { make in
                make.height.equalTo(88)
                make.top.right.equalToSuperview()
                make.left.equalToSuperview().offset(16)
            }
        
        myHeader.layer.cornerRadius = 12
        myHeader.layer.masksToBounds = true
            
        return headerView
    }
}
