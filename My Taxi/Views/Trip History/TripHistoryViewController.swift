//
//  TripHistoryViewController.swift
//  My Taxi
//
//  Created by Shokhod on 16/12/21.
//

import UIKit

class TripHistoryViewController: UIViewController {
    
    let tripData = [mockData , mockData]
    
    let sectionNames = ["6 Июля, Вторник", "5 Июля, Вторник"]
    
    var tableView: UITableView = UITableView(frame: .zero, style: .grouped)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = false
    }
}

//MARK: - Table View
extension TripHistoryViewController: UITableViewDelegate ,UITableViewDataSource {
    
    private func setUpTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "TripHistoryTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        tableView.frame = view.frame
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.backgroundColor = .white
        view.addSubview(tableView)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tripData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 134
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tripData[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TripHistoryTableViewCell

        cell.selectionStyle = .none
        cell.whereTo.text = tripData[indexPath.section][indexPath.row].wayTo
        cell.whereFrom.text = tripData[indexPath.section][indexPath.row].wayFrom
        cell.carImage.image = tripData[indexPath.section][indexPath.row].carType.image
        cell.timeLB.text = tripData[indexPath.section][indexPath.row].time
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 16, y: 0, width: tableView.frame.width, height: 50))
        
        let label = UILabel()
        label.frame = CGRect.init(x: 16, y: 0, width: headerView.frame.width, height: headerView.frame.height)
        label.frame = label.frame.inset(by: UIEdgeInsets(top: 0, left: 0, bottom: 16, right: 0))
        label.text = sectionNames[section]
        label.font = .boldSystemFont(ofSize: 30)
        label.textColor = UIColor.themeColor.textColor
        headerView.addSubview(label)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let vc = TripDetaliViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
