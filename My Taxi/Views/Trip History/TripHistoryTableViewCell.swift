//
//  TripHistoryTableViewCell.swift
//  My Taxi
//
//  Created by Shokhod on 16/12/21.
//

import UIKit

class TripHistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var whereFrom: UILabel!
    @IBOutlet weak var whereTo: UILabel!
    @IBOutlet weak var carImage: UIImageView!
    @IBOutlet weak var timeLB: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 0, left: 16, bottom: 16, right: 16))
        cellView.layer.borderWidth = 1
        cellView.layer.cornerRadius = 12
        cellView.layer.borderColor = UIColor.themeColor.borderColor?.cgColor
        
    }
    
}
