//
//  LocationInfoView.swift
//  My Taxi
//
//  Created by Shokhod on 19/12/21.
//

import UIKit

class LocationInfoView: UIView {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet var contenView: UIView!
    @IBOutlet var currentLoactionLB: UILabel!
    @IBOutlet weak var goingToGoLB: UILabel!
    @IBOutlet var nextButton: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func commonInit() {
        let viewFrameXib = Bundle.main.loadNibNamed("LocationInfoView", owner: self, options: nil)![0] as! UIView
        viewFrameXib.frame = self.frame
        addSubview(viewFrameXib)
    }
    
}
