//
//  ViewController.swift
//  My Taxi
//
//  Created by Shokhod on 16/12/21.
//

import CoreLocation
import UIKit
import GoogleMaps
import SnapKit

protocol MapViewControlllerDelegate: AnyObject {
    func didTapMenuButton()
}

class MapViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {
    
    weak var delegate: MapViewControlllerDelegate?
    
    private let manager = CLLocationManager()
    let geocoder = GMSGeocoder()
    lazy var camera = GMSCameraPosition.camera(
        withLatitude: 41.2995,
        longitude: 69.2401,
        zoom: 17
    )
    
    lazy var mapView = GMSMapView.map(withFrame: .zero, camera: camera)
    
    private lazy var mainView: UIView = {
        let view = UIView()
        return view
    }()
    
    private lazy var infoView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 24
        view.backgroundColor = .white
        return view
    }()
    
    private let markerImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "map_marker_icon")
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private lazy var userLocationView = LocationInfoView(
        frame: CGRect(
            x: .zero,
            y: .zero,
            width: view.frame.width,
            height: 105
        )
    )
    
    private lazy var menuButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .white
        button.setImage(
            UIImage(
                systemName: "text.alignleft",
                withConfiguration: UIImage.SymbolConfiguration(
                    pointSize: 18,
                    weight: .bold
                )
            ),
            for: .normal
        )
        button.imageView?.tintColor = .gray
        button.layer.cornerRadius = 20
        button.layer.borderColor = UIColor.gray.cgColor
        button.layer.borderWidth = 1
        
        button.layer.shadowColor = UIColor.black.cgColor
        button.layer.shadowOpacity = 0.5
        button.layer.shadowOffset = CGSize(width: 1, height: 1)
        button.layer.shadowRadius = 1.5
        
        return button
    }()
    
    var currentLocation: CLLocationCoordinate2D? = CLLocationCoordinate2D(latitude: 41.299496, longitude: 69.240074)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
        
        //Setting map & SetUp frames(inside)
        settingGoogleMap()
        
        menuButton.addTarget(self, action: #selector(openCloseMenu), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        userLocationView.contenView.layer.masksToBounds = true
        userLocationView.contenView.layer.cornerRadius = 12
    }
    
    //MARK: Set Frame
    private func setUpLayouts() {
        view.addSubview(mainView)
        mainView.addSubview(mapView)
        mapView.addSubview(markerImageView)
        mapView.addSubview(infoView)
        infoView.addSubview(userLocationView)
        mainView.addSubview(menuButton)
        
        mainView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        mapView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        markerImageView.snp.makeConstraints { make in
            make.width.equalTo(40)
            make.height.equalTo(50)
            make.centerY.centerX.equalToSuperview()
        }
        
        infoView.snp.makeConstraints { make in
            make.height.equalTo(147)
            make.left.right.bottom.equalToSuperview()
        }

        userLocationView.snp.makeConstraints { make in
            make.height.equalTo(105)
            make.left.equalToSuperview().offset(16)
            make.top.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.bottom.equalToSuperview().offset(-26)
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        
        menuButton.snp.makeConstraints { make in
            make.height.width.equalTo(40)
            make.top.equalTo(view.safeAreaLayoutGuide).offset(4)
            make.left.equalToSuperview().offset(16)
        }
    }
    
    //MARK: SetUp Google Map
    private func settingGoogleMap(
        coordinate: CLLocationCoordinate2D =
        CLLocationCoordinate2D(
            latitude: 41.2995,
            longitude: 69.2401
        )
    ) {
        camera = GMSCameraPosition(
            latitude: coordinate.latitude,
            longitude: coordinate.longitude,
            zoom: 17)
        mapView = GMSMapView.map(withFrame: .zero, camera: camera)
        
        // Frame setUp have to be after map SetUP
        setUpLayouts()
        
        mapView.delegate = self
        
        mapView.settings.compassButton = true
        mapView.settings.myLocationButton = true
        
        mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 120, right: 15)
        
        let position = CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude)
        let marker = GMSMarker(position: position)
        marker.map = nil
    }
    
    @objc func openCloseMenu() {
        delegate?.didTapMenuButton()
    }
}

extension MapViewController {
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        mapView.animate(toLocation: currentLocation!)
        return true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        let coordinate = location.coordinate
        currentLocation = coordinate
        settingGoogleMap(coordinate: coordinate)
    }
    
    func mapView(_ mapView: GMSMapView, idleAt cameraPosition: GMSCameraPosition) {
        geocoder.reverseGeocodeCoordinate(cameraPosition.target) { (response, error) in
            guard error == nil else {
                return
            }
            
            if let result = response?.firstResult() {
                self.userLocationView.currentLoactionLB.text = result.lines?.first
            }
        }
    }
}
